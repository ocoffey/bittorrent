use bytes::{BytesMut, Bytes};

/*
    Parts of a bencoder
    1. String coder
    2. Integer coder
    3. List coder
        * Can be recursive with lists/dictionaries inside
    4. Dictionary coder
        * Can be recursive with dictionaries/lists inside
*/



pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
