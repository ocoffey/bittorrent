// http tracker communication
use crate::{Torrent, Event, Peer};
use crate::tracker::errors::{RespError, DataError, PeerError};
use bytes::{BytesMut, Bytes};
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use bencoder as b;

impl Torrent {
    // for sending a request to the tracker
    pub fn format_request_url(&self, no_peer_id: bool, numwant: Option<u8>) -> Bytes {
        // empty bytes buffer
        let mut buf = BytesMut::new();
        // add announce url
        buf.extend_from_slice(self.meta.get_announce());
        // info hash is the first param=value pair, so it's preceeded by '?'
        // extensions are split for less overhead (no need to convert bytes to Bytes and combine)
        buf.extend_from_slice(b"?info_hash=");
        buf.extend_from_slice(&self.info_hash);
        // all other pairs are preceeded by '&'
        buf.extend_from_slice(b"&peer_id=");
        buf.extend_from_slice(&self.peer_id);
        buf.extend_from_slice(b"&port=");
        buf.extend_from_slice(&self.port);
        buf.extend_from_slice(b"&uploaded=");
        buf.extend_from_slice(&self.uploaded);
        buf.extend_from_slice(b"&downloaded=");
        buf.extend_from_slice(&self.downloaded);
        buf.extend_from_slice(b"&left=");
        buf.extend_from_slice(&self.left);
        // check for compact form
        if self.compact {
            buf.extend_from_slice(b"&compact=1");    
        } else {
            buf.extend_from_slice(b"&compact=0");
        }
        // check for peer id
        if no_peer_id {
            buf.extend_from_slice(b"&no_peer_id=1");
        } else {
            buf.extend_from_slice(b"&no_peer_id=0");
        }
        // send appropriate event info
        match &self.event {
            Event::Started  => buf.extend_from_slice(b"&event=started"),
            Event::Stopped  => buf.extend_from_slice(b"&event=stopped"),
            Event::Completed=> buf.extend_from_slice(b"&event=completed"),
            Event::None     => (),
        }
        // number of peers wanted
        // initially will default to not use this value,
        // and will find an optimal value through testing the full program later on
        match numwant {
            Some(n) => {
                buf.extend_from_slice(b"&numwant=");
                buf.extend_from_slice(&Bytes::from(n.to_string()));
            },
            None    => (),
        }
        // send key info
        match &self.key {
            Some(k) => buf.extend_from_slice(&k),
            None    => (),
        }
        // check for previous tracker id
        if let Some(b) = &self.tracker_id {
            buf.extend_from_slice(&b);
        } else {
            ()
        }
        buf.freeze() // converts the buffer to be immutable Bytes, returns it
    }

    // for understanding the response from a tracker
    // Ok return needs no data, since data will be internally written
    pub fn parse_response(&mut self, response: Bytes) -> Result<(), RespError> {
        // decode the response from bencoded format
        let parsed_response = b::Parsed::new(response);
        match parsed_response {
            Ok(r)  => if let Some(b::ParseOptions::Dictionary(d)) = r.structure.get(0) {
                // check for failure reason
                match d.get(self.resp[0]) {
                    Some(b::ParseOptions::ByteString(f)) => return Err(RespError::TrackerFailure(String::from_utf8_lossy(f).to_string())),
                    _ => (),
                }
                // check for warning message
                match d.get(self.resp[1]) {
                    Some(b::ParseOptions::ByteString(w)) => eprintln!("{}", String::from_utf8_lossy(w)),
                    _ => (),
                }
                // check for interval
                self.interval = match d.get(self.resp[2]) {
                    Some(b::ParseOptions::Integer(i)) => Some(i.to_owned()),
                    _ => return Err(RespError::LackingData(DataError::NoInterval)),
                };
                // check for min interval
                self.min_interval = match d.get(self.resp[3]) {
                    Some(b::ParseOptions::Integer(mi)) => Some(mi.to_owned()),
                    _ => None,
                };
                // check for tracker id
                match d.get(self.resp[4]) {
                    Some(b::ParseOptions::ByteString(t)) => self.tracker_id = Some(t.to_owned()),
                    _ => (),
                }
                // check for complete
                self.complete = match d.get(self.resp[5]) {
                    Some(b::ParseOptions::Integer(c)) => Some(c.to_owned()),
                    _ => return Err(RespError::LackingData(DataError::NoSeeders)),
                };
                // check for incomplete
                self.incomplete = match d.get(self.resp[6]) {
                    Some(b::ParseOptions::Integer(ic)) => Some(ic.to_owned()),
                    _ => return Err(RespError::LackingData(DataError::NoLeechers)),
                };
                // check for peers
                self.peers = match d.get(self.resp[7]) {
                    Some(b::ParseOptions::List(p)) => Torrent::fill_peers(self.peer_k, p)?,
                    Some(b::ParseOptions::ByteString(p)) => Torrent::fill_peers_b(p)?,
                    _ => return Err(RespError::LackingData(DataError::NoPeerList)),
                };
                Ok(())
            } else {
                Err(RespError::NoDict)
            },
            Err(e) => Err(RespError::DecodeError(e.to_string())),
        }
    }

    // helper method for filling the peers list
    // list of dictionaries
    pub fn fill_peers(keys: [&'static [u8]; 3], peers: &Vec<b::ParseOptions>) -> Result<Option<Vec<Peer>>, PeerError> {
        // empty vector to hold peers
        let mut temp = Vec::new();
        // for each peer
        for peer in peers {
            // destructure the peer dictionary
            if let b::ParseOptions::Dictionary(d) = peer {
                temp.push(
                    Peer {
                        peer_id: match d.get(keys[0]) {
                            Some(b::ParseOptions::ByteString(pi)) => Some(pi.to_owned()),
                            _ => None,
                        },
                        ip: match d.get(keys[1]) {
                            Some(b::ParseOptions::ByteString(ip)) => match String::from_utf8_lossy(ip).parse() {
                                Ok(IpAddr::V4(a)) => IpAddr::V4(a),
                                Ok(IpAddr::V6(b)) => IpAddr::V6(b),
                                Err(_) => return Err(PeerError::IPParseErr),
                            },
                            _ => return Err(PeerError::NoIP),
                        },
                        port: match d.get(keys[2]) {
                            Some(b::ParseOptions::Integer(i)) => Bytes::from(i.to_string()),
                            _ => return Err(PeerError::NoPort),
                        }, 
                    }
                );
            } else {
                return Err(PeerError::NoPeerDict);
            }
        }
        Ok(Some(temp))
    }

    pub fn fill_peers_b(peers: &Bytes) -> Result<Option<Vec<Peer>>, PeerError> {
        // check to ensure the peer list is valid
        if peers.len() % 6 != 0 {
            return Err(PeerError::InvalidList)
        }
        // for holding the peers
        let mut temp = Vec::new();
        // for the binary model, peers is a concatenated string,
        // where the first 4 bytes are the ipv4 address, and the last 2 are the port
        // it is a multiple of 6 bytes long
        // the binary model lacks peer id's
        for x in 0..(peers.len()/6) {
            temp.push(
                Peer {
                    peer_id: None,
                    // destructures each get() safely
                    ip: IpAddr::V4(Ipv4Addr::new(
                        match peers.get(x*6) {
                            Some(a) => *a,
                            None => return Err(PeerError::IPSliceErr),
                        },
                        match peers.get((x*6)+1) {
                            Some(b) => *b,
                            None => return Err(PeerError::IPSliceErr),
                        },
                        match peers.get((x*6)+2) {
                            Some(c) => *c,
                            None => return Err(PeerError::IPSliceErr),
                        },
                        match peers.get((x*6)+3) {
                            Some(d) => *d,
                            None => return Err(PeerError::IPSliceErr),
                        },
                    )),
                    port: {
                        // destructure separate get()s
                        // converts to appropriate u16 places
                        // convert to string, then to Bytes
                        let mut temp = 0;
                        if let Some(e) = peers.get((x*6)+4) {
                            temp += (*e as u16) << 8;
                        } else {
                            panic!()
                        }
                        if let Some(f) = peers.get((x*6)+5) {
                            temp = temp | (*f as u16);
                        } else {
                            panic!()
                        }
                        Bytes::from(temp.to_string())
                    },
                }
            );
        }
        Ok(Some(temp))
    }
}
