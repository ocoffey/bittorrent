pub mod http_t;
pub mod http_s;

#[cfg(test)]
mod tests {
    use super::*;
    use metainfo as m;
    use crate::{Torrent, Event, Peer};
    use bytes::{BytesMut, Bytes};
    use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
    use bencoder as b;
    use crate::tracker::errors::{RespError, ScrapeError};

    // statics used as keys to the 'response' dictionary
    static RESPONSE: [&'static [u8]; 8] = [
        b"failure reason",
        b"warning message",
        b"interval",
        b"min interval",
        b"tracker id",
        b"complete",
        b"incomplete",
        b"peers",
    ];
    // statics used as keys to the 'peer' dictionary
    static PEERKEYS: [&'static [u8]; 3] = [
        b"peer id",
        b"ip",
        b"port",
    ];
    // statics used as keys to the scrape response dictionary
    static SCRAPEKEYS: [&'static [u8]; 6] = [
        b"files",
        b"complete",
        b"incomplete",
        b"failure reason",
        b"flags",
        b"min_request_interval",
    ];

    #[test]
    fn test_request() {
        // make metadata struct
        // self peer id, port, ip, key all passed
        // make torrent with this
        // to request, pass no_peer_id, numwant
        let meta_input = Bytes::from(&b"d8:announce33:udp://tracker.opentrackr.org:13377:comment12:ManjaroLinux10:created by13:mktorrent 1.113:creation datei1583936924e4:infod6:lengthi0e4:name37:manjaro-kde-19.0.2-200311-linux54.iso12:piece lengthi0e6:pieces0:ee"[..]);
        let meta_parse = b::Parsed::new(meta_input);
        if let Ok(mp) = meta_parse {
            let meta = m::MetaInfo::new(&mp);
            if let Ok(m) = meta {
                static IPEX: IpAddr = IpAddr::V4(Ipv4Addr::new(172, 16, 254, 1));
                static KEYEX: Option<Bytes> = None;
                let torrent = Torrent::new(Bytes::from(&b"fakepeer_id111111111"[..]), Bytes::from("87"), &IPEX, &KEYEX, RESPONSE, PEERKEYS, SCRAPEKEYS, m);
                let request = torrent.format_request_url(true, None);
                assert_eq!(String::from_utf8_lossy(&request), "udp://tracker.opentrackr.org:1337?info_hash=%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38&peer_id=fakepeer_id111111111&port=87&uploaded=0&downloaded=0&left=0&compact=0&no_peer_id=1&event=started");
            } else {
                panic!("Couldn't parse bencoded data to metainfo.")
            }

        } else {
            panic!("Failed to parse bencoded data.")
        }
    }

    #[test]
    fn test_response() {
        // make one torrent, have separate responses occur
        // input for bencoded decoding
        let meta_input = Bytes::from(&b"d8:announce33:udp://tracker.opentrackr.org:13377:comment12:ManjaroLinux10:created by13:mktorrent 1.113:creation datei1583936924e4:infod6:lengthi0e4:name37:manjaro-kde-19.0.2-200311-linux54.iso12:piece lengthi0e6:pieces0:ee"[..]);
        // putting into the bencoder
        let meta_parse = b::Parsed::new(meta_input);
        // decoding
        if let Ok(mp) = meta_parse {
            // converting struct to metainfo struct
            let meta = m::MetaInfo::new(&mp);
            // if successful
            if let Ok(m) = meta {
                // make torrent with this metainfo
                static IPEX: IpAddr = IpAddr::V4(Ipv4Addr::new(167, 11, 233, 1));
                static KEYEX: Option<Bytes> = None;
                let mut torrent = Torrent::new(Bytes::from(&b"fakepeer_idtestnowkk"[..]), Bytes::from("101"), &IPEX, &KEYEX, RESPONSE, PEERKEYS, SCRAPEKEYS, m);
                // failure reason test
                // bencoded dictionary with one element (failure reason)
                match torrent.parse_response(Bytes::from(&b"d14:failure reason15:you done goofede"[..])) {
                    Err(e) => assert_eq!(RespError::TrackerFailure(String::from("you done goofed")), e),
                    Ok(_) => panic!("Incorrent Response"),
                }
                // ok test
                // sample torrent struct to test against
                let test_tor = Torrent {
                    peer_id: Bytes::from(&b"fakepeer_idtestnowkk"[..]),
                    port: Bytes::from(&b"101"[..]),
                    ip: &IPEX,
                    key: &KEYEX,
                    uploaded: Bytes::from(&b"0"[..]),
                    downloaded: Bytes::from(&b"0"[..]),
                    left: Bytes::from(&b"0"[..]),
                    compact: false,
                    tracker_id: Some(Bytes::from(&b"sample"[..])),
                    event: Event::Started,
                    peers: Some(
                        vec![
                            Peer {
                                ip: IpAddr::V4(Ipv4Addr::new(1, 1, 1, 1)),
                                peer_id: None,
                                port: Bytes::from(&b"11"[..]),
                            }
                        ]
                    ),
                    interval: Some(5),
                    min_interval: None,
                    min_scrape: None,
                    scrape_announce: None,
                    complete: Some(17),
                    incomplete: Some(23),
                    info_hash: Bytes::from(&b"%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38"[..]),
                    meta: torrent.meta.clone(),
                    resp: RESPONSE,
                    peer_k: PEERKEYS,
                    scrape_k: SCRAPEKEYS,
                };
                match torrent.parse_response(Bytes::from(&b"d8:completei17e10:incompletei23e8:intervali5e5:peersld2:ip7:1.1.1.14:porti11eee10:tracker id6:samplee"[..])) {
                    Ok(_)  => assert_eq!(torrent, test_tor),
                    Err(e) => panic!("Failed with error: {}", e.to_string()),
                }
                // warning test
                // for simplicity, just running cargo test with '-- --nocapture'
                // to ensure that the error message is printed to stderr
                match torrent.parse_response(Bytes::from(&b"d8:completei17e10:incompletei23e8:intervali5e5:peersld2:ip7:1.1.1.14:porti11eee10:tracker id6:sample15:warning message8:an errore"[..])) {
                    Ok(_)  => assert_eq!(torrent, test_tor),
                    Err(e) => panic!("Failed with error: {}", e.to_string()),
                }
            } else {
                panic!("Couldn't parse bencoded data to metainfo.")
            }
        } else {
            panic!("Failed to parse bencoded data.");
        }
    }

    #[test]
    fn test_scrape_i() {
        // using examples from the specification page
        let mut urls = Vec::new();
        urls.push((Bytes::from("http://example.com/announce"), Some(Bytes::from("http://example.com/scrape"))));
        urls.push((Bytes::from("http://example.com/x/announce"), Some(Bytes::from("http://example.com/x/scrape"))));
        urls.push((Bytes::from("http://example.com/announce.php"), Some(Bytes::from("http://example.com/scrape.php"))));
        urls.push((Bytes::from("http://example.com/a"), None));
        urls.push((Bytes::from("http://example.com/announce?x2%0644"), Some(Bytes::from("http://example.com/scrape?x2%0644"))));
        urls.push((Bytes::from("http://example.com/announce?x=2/4"), None));
        urls.push((Bytes::from("http://example.com/x%064announce"), None));
        // for each input announce url
        for (url, correct) in urls {
            assert_eq!(Torrent::scrape_init(&url), correct);
        }
    }

    #[test]
    fn test_scrape_r() {
        // make one torrent, have separate responses occur
        // input for bencoded decoding
        let meta_input = Bytes::from(&b"d8:announce42:udp://tracker.opentrackr.org/announce:13377:comment12:ManjaroLinux10:created by13:mktorrent 1.113:creation datei1583936924e4:infod6:lengthi0e4:name37:manjaro-kde-19.0.2-200311-linux54.iso12:piece lengthi0e6:pieces0:ee"[..]);
        // putting into the bencoder
        let meta_parse = b::Parsed::new(meta_input);
        // decoding
        if let Ok(mp) = meta_parse {
            // converting struct to metainfo struct
            let meta = m::MetaInfo::new(&mp);
            // if successful
            if let Ok(m) = meta {
                // make torrent with this metainfo
                static IPEX: IpAddr = IpAddr::V4(Ipv4Addr::new(167, 11, 233, 1));
                static KEYEX: Option<Bytes> = None;
                let mut torrent = Torrent::new(Bytes::from(&b"fakepeer_idtestnowkk"[..]), Bytes::from("101"), &IPEX, &KEYEX, RESPONSE, PEERKEYS, SCRAPEKEYS, m);
                // fails when it should
                match torrent.scrape_response(Bytes::from("d14:failure reason16:this should faile")) {
                    Err(e) => assert_eq!(e, ScrapeError::ScrapeFailure(String::from("this should fail"))),
                    Ok(_) => panic!("Failure Reason rest should have failed."),
                }
                // mutable torrent to assert against
                // everything set to None where needed, first test is the min_scrape interval
                let mut test_tor = Torrent {
                    peer_id: Bytes::from(&b"fakepeer_idtestnowkk"[..]),
                    port: Bytes::from(&b"101"[..]),
                    ip: &IPEX,
                    key: &KEYEX,
                    uploaded: Bytes::from(&b"0"[..]),
                    downloaded: Bytes::from(&b"0"[..]),
                    left: Bytes::from(&b"0"[..]),
                    compact: false,
                    tracker_id: None,
                    event: Event::Started,
                    peers: None,
                    interval: None,
                    min_interval: None,
                    min_scrape: Some(2),
                    scrape_announce: Some(Bytes::from("udp://tracker.opentrackr.org/scrape:1337")),
                    complete: None,
                    incomplete: None,
                    info_hash: Bytes::from(&b"%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38"[..]),
                    meta: torrent.meta.clone(),
                    resp: RESPONSE,
                    peer_k: PEERKEYS,
                    scrape_k: SCRAPEKEYS,
                };

                // min request interval testing
                match torrent.scrape_response(Bytes::from("d5:flagsd20:min_request_intervali2eee")) {
                    Err(_) => assert_eq!(torrent, test_tor),
                    Ok(_)  => panic!("Min Request test should have failed."),
                }

                // updated with complete and incomplete amounts
                test_tor = Torrent {
                    peer_id: Bytes::from(&b"fakepeer_idtestnowkk"[..]),
                    port: Bytes::from(&b"101"[..]),
                    ip: &IPEX,
                    key: &KEYEX,
                    uploaded: Bytes::from(&b"0"[..]),
                    downloaded: Bytes::from(&b"0"[..]),
                    left: Bytes::from(&b"0"[..]),
                    compact: false,
                    tracker_id: None,
                    event: Event::Started,
                    peers: None,
                    interval: None,
                    min_interval: None,
                    min_scrape: Some(2),
                    scrape_announce: Some(Bytes::from("udp://tracker.opentrackr.org/scrape:1337")),
                    complete: Some(34),
                    incomplete: Some(22),
                    info_hash: Bytes::from(&b"%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38"[..]),
                    meta: torrent.meta.clone(),
                    resp: RESPONSE,
                    peer_k: PEERKEYS,
                    scrape_k: SCRAPEKEYS,
                };

                // testing for updating complete and incomplete amounts
                match torrent.scrape_response(Bytes::from("d5:filesd47:%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38d8:completei34e10:incompletei22eeee")) {
                    Ok(()) => assert_eq!(torrent, test_tor),
                    Err(e) => panic!("Should have passed and didn't with error: {}", e.to_string()),
                }
            } else {
                panic!("Couldn't parse bencoded data to metainfo.")
            }
        } else {
            panic!("Failed to parse bencoded data.");
        }
    }
}