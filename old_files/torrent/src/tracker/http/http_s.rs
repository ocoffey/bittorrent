// http scraping methods
use crate::Torrent;
use crate::tracker::errors::{ScrapeError};
use bytes::Bytes;
use regex::bytes::Regex;
use bencoder as b;

impl Torrent {
    pub(crate) fn scrape_init(announce: &Bytes) -> Option<Bytes> {
        // regex for matching the announce
        lazy_static! {
            // matches on forward slashes - '/'
            static ref SL: Regex = Regex::new(r"/").unwrap();
            // matches on 'announce'
            static ref AN: Regex = Regex::new(r"(announce)").unwrap();
        }
        // matches for last '/'
        if let Some(match_sl) = SL.find_iter(announce).last() {
            // checks for 'announce' listed after the last '/'
            if let Some(match_an) = AN.find_at(announce, match_sl.end()) {
                // if 'announce' occurs directly after the last '/'
                if match_an.start() == match_sl.end() {
                    // then it's a valid tracker for scraping
                    Some(Bytes::from(AN.replace(announce, &b"scrape"[..]).to_vec()))
                } else {
        // invalid for all other cases
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
        
    }

    pub fn scrape_response(&mut self, response: Bytes) -> Result<(), ScrapeError> {
        // parse the dictionary
        let parsed_response = b::Parsed::new(response);
        match parsed_response {
            // if successfully parsed, get the first element in the structure
            Ok(r) => if let Some(b::ParseOptions::Dictionary(d)) = r.structure.get(0) {
                // initially check for other flags
                if let Some(b::ParseOptions::Dictionary(fl)) = d.get(self.scrape_k[4]) {
                    // if there's a min scrape request interval
                    if let Some(b::ParseOptions::Integer(mri)) = fl.get(self.scrape_k[5]) {
                        // set the interval
                        self.min_scrape = Some(mri.to_owned());
                    }
                }
                // if we get a dictionary to our 'files' key
                if let Some(b::ParseOptions::Dictionary(file)) = d.get(self.scrape_k[0]) {
                    // if we get a dictionary to our info hash as a key
                    if let Some(b::ParseOptions::Dictionary(data)) = file.get(&self.info_hash) {
                        // match complete from the dictionary
                        match data.get(self.scrape_k[1]) {
                            Some(b::ParseOptions::Integer(c)) => self.complete = Some(c.to_owned()),
                            _ => return Err(ScrapeError::NoComplete),
                        }
                        // match incomplete from the dictionary
                        match data.get(self.scrape_k[2]) {
                            Some(b::ParseOptions::Integer(ic)) => self.incomplete = Some(ic.to_owned()),
                            _ => return Err(ScrapeError::NoIncomplete),
                        }
                        Ok(())
                    } else {
                        // wrong info hash/info hash didn't work
                        return Err(ScrapeError::InfoKey)
                    }
                } else {
                    // if instead the scrape failed, and we have a failure reason
                    if let Some(b::ParseOptions::ByteString(fr)) = d.get(self.scrape_k[3]) {
                        return Err(ScrapeError::ScrapeFailure(String::from_utf8_lossy(fr).to_string()))
                    } else {
                        return Err(ScrapeError::UnknownError)
                    }
                }
            } else {
                Err(ScrapeError::NoResponse)
            },
            // potentially adding more error types later and reworking this
            Err(e) => Err(ScrapeError::ParseError(e.to_string())),
        }
    }
}