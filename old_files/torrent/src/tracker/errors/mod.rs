use std::fmt;

// error types for tracker interaction

// custom error type for Response Errors
#[derive(Eq, PartialEq, Debug)]
pub enum RespError {
    TrackerFailure(String), // the tracker replies with an error message
    LackingData(DataError), // needed pieces of the tracker response are missing
    PeerErr(PeerError),     // there was an error reading the peer dictionary
    NoDict,                 // the tracker did not give a bencoded dictionary reply
    DecodeError(String),    // the reply could not be decoded properly
}

// formatting for the errors
impl fmt::Display for RespError {
    fn fmt(&self, f:&mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RespError::TrackerFailure(fm) => write!(f, "Tracker responded with failure reason: {}", fm),
            RespError::LackingData(de)    => write!(f, "Response lacked certain data: {}", de),
            RespError::PeerErr(pe)        => write!(f, "Peer dictionary gave error: {}", pe),
            RespError::NoDict             => write!(f, "Response was not a bencoded dictionary."),
            RespError::DecodeError(d)     => write!(f, "Could not decode response with error: {}", d),
        }
    }
}

// for easier error propogation of Peer Errors
impl From<PeerError> for RespError {
    fn from(error: PeerError) -> Self {
        RespError::PeerErr(error)
    }
}

// built upon the Lacking Data error, to give a more specific reason
#[derive(Eq, PartialEq, Debug)]
pub enum DataError {
    NoInterval,
    NoSeeders,
    NoLeechers,
    NoPeerList,
}

impl fmt::Display for DataError {
    fn fmt(&self, f:&mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            DataError::NoInterval => write!(f, "No interval given from the tracker."),
            DataError::NoSeeders  => write!(f, "No seeders data given from the tracker."),
            DataError::NoLeechers => write!(f, "No leechers data given from the tracker."),
            DataError::NoPeerList => write!(f, "No peer list given from the tracker."),
        }
    }
}

// specific errors from reading the peer dictionary
#[derive(Eq, PartialEq, Debug)]
pub enum PeerError {
    NoIP,
    NoPort,
    NoPeerDict,
    IPParseErr,
    InvalidList,
    IPSliceErr,
    PortSliceErr,
}

impl fmt::Display for PeerError {
    fn fmt(&self, f:&mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            PeerError::NoIP         => write!(f, "No IP address listed for peer."),
            PeerError::NoPort       => write!(f, "No port number listed for peer."),
            PeerError::NoPeerDict   => write!(f, "No peer dictionary listed."),
            PeerError::IPParseErr   => write!(f, "Failure to parse IP address."),
            PeerError::InvalidList  => write!(f, "Invalid binary peer list."),
            PeerError::IPSliceErr   => write!(f, "Error getting binary slice."),
            PeerError::PortSliceErr => write!(f, "Error converting port number."),
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub enum ScrapeError {
    InfoKey,
    NoComplete,
    NoIncomplete,
    NoResponse,
    ParseError(String),
    ScrapeFailure(String),
    UnknownError,
}

impl fmt::Display for ScrapeError {
    fn fmt(&self, f:&mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ScrapeError::InfoKey            => write!(f, "Info hash as key gave no results."),
            ScrapeError::NoComplete         => write!(f, "No complete number given."),
            ScrapeError::NoIncomplete       => write!(f, "No incomplete number given."),
            ScrapeError::NoResponse         => write!(f, "Improper response received."),
            ScrapeError::ParseError(pe)     => write!(f, "Parse error occurred with error: {}", pe),
            ScrapeError::ScrapeFailure(sf)  => write!(f, "Scrape failed with error message: {}", sf),
            ScrapeError::UnknownError       => write!(f, "Scrape failed with unknown error."),
        }
    }
}