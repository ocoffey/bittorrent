use bencoder as b;
use metainfo as m;
pub mod tracker;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use bytes::{Bytes, BytesMut};
use std::convert::From;
#[macro_use]
extern crate lazy_static;

#[derive(Debug, Eq, PartialEq)]
struct Torrent {
    // immutable referenced client info
    peer_id: Bytes,    // unique 20 byte string for the client (reference client's)
    port: Bytes,       // reference client
    ip: &'static IpAddr,        // reference client
    key: &'static Option<Bytes>,// reference client
    resp: [&'static [u8]; 8],   // predetermined response dictionary inputs
    peer_k: [&'static [u8]; 3], // predetermined keys for peer dictionaries
    scrape_k: [&'static [u8]; 6],
    // mutable torrent info for requests
    uploaded: Bytes,            // base 10 encoded, amount uploaded in bytes
    downloaded: Bytes,          // base 10 encoded
    left: Bytes,                // base 10 encoded
    compact: bool,              // actually 1 or 0, so accomodate that
    tracker_id: Option<Bytes>,  // if previously given, should be listed here (initially unknown)
    event: Event,               // state of the torrent (initially Started)
    // mutable torrent info from responses
    peers: Option<Vec<Peer>>,   // dictionary of all peers (parsed into a list)
    interval: Option<i64>,    // u8 (interval in seconds to send requests)
    min_interval: Option<i64>,// u8 (min interval in seconds, don't send sooner than this)
    complete: Option<i64>,    // usize (number of seeders)
    incomplete: Option<i64>,  // usize (number of leechers)
    // mutable info from scrape responses
    min_scrape: Option<i64>,
    // immutable torrent info
    scrape_announce: Option<Bytes>,
    info_hash: Bytes,   // SHA1 hash of info value from bencoded dict
    meta: m::MetaInfo,  // metainfo for the torrent
}

// Singular peer struct
#[derive(Debug, PartialEq, Eq, Clone)]
struct Peer {
    peer_id: Option<Bytes>, // peer id (None in compact mode)
    ip: IpAddr,             // ip address of peer
    port: Bytes,            // utf8 Bytes converted from u16 (for ease of using in other sections)
}

#[derive(Debug, Eq, PartialEq)]
// Event enum for message passing and torrent's state
enum Event {
    Started,    // First request to the tracker (occurs on program start, and upon new torrent)
    Stopped,    // Sent to the tracker for graceful shutdown
    Completed,  // Sent to the tracker upon download completion (not sent if the torrent was previously completed)
    None,       // Unspecified event (event not always needed)
}

impl Torrent {
    pub fn new(peerid: Bytes, port: Bytes, ip: &'static IpAddr, key: &'static Option<Bytes>, response: [&'static [u8]; 8], peerkey: [&'static [u8]; 3], scrapekey:[&'static [u8]; 6], metadata: m::MetaInfo) -> Torrent {
        Torrent {
            peer_id: Torrent::url_encode(&peerid),  // url encode the client's peer id
            port: port,                             // reference the client's port number
            ip: ip,                                // reference the client's ip
            key: key,                              // reference the client's key
            resp: response,
            peer_k: peerkey,
            scrape_k: scrapekey,
            uploaded: Bytes::from_static(b"0"),     // starts at 0
            downloaded: Bytes::from_static(b"0"),   // starts at 0
            left: Bytes::from(metadata.get_len()),  // for single torrent file, reads length. Calculates for multi torrent
            compact: false,         // set to false, might give option later? (through client)
            tracker_id: None,       // initially none (received from the tracker)
            event: Event::Started,  // initially set to Started
            peers: None,            // obtained from the tracker
            interval: None,         // obtained from the tracker 
            min_interval: None,     // obtained from the tracker
            complete: None,         // obtained from the tracker
            incomplete: None,       // obtained from the tracker
            min_scrape: None,
            scrape_announce: Torrent::scrape_init(metadata.get_announce()),
            info_hash: Torrent::url_encode(metadata.get_hash()),// url encode the torrent's info hash
            meta: metadata,         // store the metadata for the torrent
        }
    }

    // for url-encoding the info hash and peer id
    // non listed characters need to be escaped with '%', and listed as their hex value
    fn url_encode(input: &Bytes) -> Bytes {
        let mut buf: Vec<u8> = Vec::new();
        for byte in input {
            match byte {
                // any of these characters are safe to push
                b'a'..=b'z' | b'A'..=b'Z' |
                b'0'..=b'9' | b'.' |
                b'-' | b'_' | b'~' => buf.push(*byte),
                _ => {
                    // add the % escape character
                    buf.extend_from_slice("%".as_bytes());
                    // convert the bytes to uppercase hex, then to appropriate bytes
                    buf.extend_from_slice(format!("{:X?}", byte).as_bytes());
                },
            }
        }
        // convert the vector to Bytes
        Bytes::from(buf)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static RESPONSE: [&'static [u8]; 8] = [
        b"failure reason",
        b"warning message",
        b"interval",
        b"min interval",
        b"tracker id",
        b"complete",
        b"incomplete",
        b"peers",
    ];
    // statics used as keys to the 'peer' dictionary
    static PEERKEYS: [&'static [u8]; 3] = [
        b"peer id",
        b"ip",
        b"port",
    ];
    // statics used as keys to the scrape response dictionary
    static SCRAPEKEYS: [&'static [u8]; 6] = [
        b"files",
        b"complete",
        b"incomplete",
        b"failure reason",
        b"flags",
        b"min_request_interval",
    ];

    #[test]
    fn test_url_encode() {
        let a = Bytes::from("hello!");
        let encoded = Torrent::url_encode(&a);
        assert_eq!(encoded, Bytes::from("hello%21"));
    }

    #[test]
    fn test_peers() {
        // list of dictionaries
        // dictionaries have the keys "peerid", "ip", "port"
        // peer id can be missing
        let input = Bytes::from(&b"ld2:ip12:172.16.254.17:peer id20:jfj3jdhs9g9fj3hd9dsb4:porti76eed2:ip23:2001:db8::8a2e:370:73344:porti6667eee"[..]);
        let mut comp_input = Vec::new();
        comp_input.push(Peer {
            peer_id: Some(Bytes::from("jfj3jdhs9g9fj3hd9dsb")),
            ip: IpAddr::V4(Ipv4Addr::new(172, 16, 254, 1)),
            port: Bytes::from("76"),
        });
        comp_input.push(Peer {
            peer_id: None,
            ip: IpAddr::V6(Ipv6Addr::new(0x2001, 0xdb8, 0, 0, 0, 0x8a2e, 0x370, 0x7334)),
            port: Bytes::from("6667"),
        });
        let parse = b::Parsed::new(input);
        static PEERKEYS: [&'static [u8]; 3] = [
            b"peer id",
            b"ip",
            b"port",
        ];
        match parse {
            Ok(p) => {
                let mut peers = Vec::new();
                if let Some(b::ParseOptions::List(l)) = p.structure.get(0) {
                    if let Ok(Some(list)) = Torrent::fill_peers(PEERKEYS, l) {
                        peers.extend_from_slice(&list);
                    } else {
                        panic!("Invalid peer filling")
                    }
                } else {
                    panic!("Invalid Parsing");
                }
                assert_eq!(peers.get(0), comp_input.get(0));
                assert_eq!(peers.get(1), comp_input.get(1));
                ()
            },
            Err(e) => panic!("Failed with error: {}", e.to_string()),
        }
    }
    
    #[test]
    fn test_peers_b() {
        // bytestring of inputs
        // multiple of '6' long
        // first 4 bytes are ipv4 address, last 2 bytes are port (no peer id)
        let temp: Vec<u8> = vec![172, 16, 254, 1, 0, 76, 201, 11, 134, 1, 26, 198];
        let mut comp_input = Vec::new();
        comp_input.push(Peer {
            peer_id: None,
            ip: IpAddr::V4(Ipv4Addr::new(172, 16, 254, 1)),
            port: Bytes::from("76"),
        });
        comp_input.push(Peer {
            peer_id: None,
            ip: IpAddr::V4(Ipv4Addr::new(201, 11, 134, 1)),
            port: Bytes::from("6854"),
        });
        let mut temp2 = BytesMut::with_capacity(15);
        temp2.extend_from_slice(&b"12:"[..]);
        temp2.extend_from_slice(&temp);
        let parse = b::Parsed::new(temp2.freeze());
        match parse {
            Ok(p)  => {
                let mut peers = Vec::new();
                match p.structure.get(0) {
                    Some(b::ParseOptions::ByteString(b)) |
                    Some(b::ParseOptions::ByteString(b)) => match Torrent::fill_peers_b(&b) {
                        Ok(Some(list)) => peers.extend_from_slice(&list),
                        Err(e) => panic!("Couldn't fill binary peers with error: {}", e.to_string()),
                        _ => panic!("Error filling list."),
                    },
                    _ => panic!("Invalid Parsing"),
                }
                assert_eq!(peers.get(0), comp_input.get(0));
                assert_eq!(peers.get(1), comp_input.get(1));
            },
            Err(e) => panic!("Failed with error: {}", e.to_string()),
        }
    }

    #[test]
    fn test_torrent() {
        let meta_input = Bytes::from(&b"d8:announce33:udp://tracker.opentrackr.org:13377:comment12:ManjaroLinux10:created by13:mktorrent 1.113:creation datei1583936924e4:infod6:lengthi0e4:name37:manjaro-kde-19.0.2-200311-linux54.iso12:piece lengthi0e6:pieces0:ee"[..]);
        let meta_parse = b::Parsed::new(meta_input);
        if let Ok(mp) = meta_parse {
            let meta = m::MetaInfo::new(&mp);
            if let Ok(m) = meta {
                static IPEX: IpAddr = IpAddr::V4(Ipv4Addr::new(171, 16, 254, 1));
                static KEYEX: Option<Bytes> = None;
                let torrent = Torrent::new(Bytes::from(&b"fakepeer_idimpllater"[..]), Bytes::from("99"), &IPEX, &KEYEX, RESPONSE, PEERKEYS, SCRAPEKEYS, m.clone());
                let test_tor = Torrent {
                    info_hash: Bytes::from("%A0%E%60%F7%C5v%18%12%90%99%5E%1F%11BOVg%99%C38"),
                    meta: m,
                    peer_id: Bytes::from(&b"fakepeer_idimpllater"[..]),
                    port: Bytes::from("99"),
                    ip: &IPEX,
                    key: &KEYEX,
                    uploaded: Bytes::from("0"),
                    downloaded: Bytes::from("0"),
                    left: Bytes::from("0"),
                    compact: false,
                    tracker_id: None,
                    event: Event::Started,
                    peers: None,
                    interval: None,
                    min_interval: None,
                    min_scrape: None,
                    scrape_announce: None,
                    complete: None,
                    incomplete: None,
                    resp: RESPONSE,
                    peer_k: PEERKEYS,
                    scrape_k: SCRAPEKEYS,
                };
                assert_eq!(torrent, test_tor);
            }
        }
    }
}
