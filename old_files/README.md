# Bittorrent

[![pipeline status](https://gitlab.com/ocoffey/bittorrent/badges/master/pipeline.svg)](https://gitlab.com/ocoffey/bittorrent/-/commits/master)

Goal: An open source Bittorrent Client made in Rust.

Below are the current development phases for this project.

## 0.1 - [ ] - Completing the Bittorrent Specification

As described [here](https://wiki.theory.org/index.php/BitTorrentSpecification). The specification is for the 1.0 bittorrent protocol.

### 0.1.1 - [X] - Bencoding Parser

Functionality for parsing bencoded input to a struct. Stored in a separate repository ([here](https://gitlab.com/ocoffey/bencoder)).

* Future - rework to also support encoding a struct to the bencoded format.

### 0.1.2 - [X] - MetaInfo Struct

A struct that can take bencoding parsed input and returns a relevant struct for a bittorrent client.

* Future - rework to include more encoding options for hashing types.

### 0.1.3 - [X] - HTTP/HTTPS Protocol Support

Functionality for sending appropriately formatted GET requests, and making sense of the server's responses. Used when starting a bittorrent download, and throughout the downloading and uploading processes.

#### 0.1.3.6 - [X] - Tracker Scrape Convention

Support for scrape requests to trackers. Used for updating peer data in the client (ie. amount of seeders and leechers per torrent).

### 0.1.3.6 - [ ] - UDP Protocol Support

Support for connecting to a tracker over UDP.

### 0.1.4 - [ ] - Client

Initial stage of the networking client. Beginning tests will connect to the tracker and check that the HTTP/HTTPS formatting section is working.

### 0.1.4.5 - [ ] - Peer Wire Protocol (TCP)

The main messaging and data transfer method between peers. Primarily used to request a piece transfer from other peers, and to let them know when you obtained a piece.

### 0.1.5 - [ ] - Algorithms

Support for algorithms used within the Peer Wire Protocol. Most of these algorithms are standard within bittorrent clients, and further algorithm generation will be explored.

### 0.1.6 - [ ] - Official Protocol Extensions

Extensions that occurred past the 1.0 published protocol. Includes Fast Peers, Distributed Hash Table, and Connection Obfuscation.

### 0.1.7 - [ ] - Unnofficial Protocol Extensions

Extensions that mostly occur within specific bittorrent clients. Inter-client support should be handled.

## 0.2 - [ ] - User Interface Functionality

Initial design for this program is for making Command Line Interface functionality, with possible GUI functionality at a later time.

### 0.2.1 - [ ] - Opening of and Creation of .torrent files

Basic functionality for a bittorrent client.

### 0.2.2 - [ ] - Pausing/Resuming bittorrent connections

Further basic functionality for clients. Additional features to this would be "smart" connections (automatical pausing on client shutdown, and automatic resuming on client startup).

### 0.2.3 - [ ] - Bittorrent Statistics

Stats on each bittorrent connection. Some example stats include:

* Completion percentage
* Download/Upload Speed
* Download/Upload Ratio

## 0.3 - [ ] - Additional Functionalities

### 0.3.1 - [ ] - Virtual Private Network Routing

Ability to route some or all connections through a VPN.

### 0.3.2 - [ ] - I2P Protocol Support

As far as is noted, Azereus/Vuze is the only bittorrent client to support this protocol. I believe more methods of encryption should be a future goal of bittorrent client designers, and so will be attempting to include support for this protocol.

### 0.3.3 - [ ] - Inter-Client File Transfer

I would like to implement file transfer functionality between multiple instances of the application, using encrypted/secure methods for doing so. The use case of this functionality would be smaller scale file transfer, whether between friends and family, computers within a corporate network, and other like situations. Depending on its traction, it may be spun out into a separate application.

### 0.3.4 - [ ] - Web Client Functionality

A possible functionality for the client; allowing users to connect to a website for this project, and use the client within a browser. It would be beneficial for short term downloading needs, and would likely use WebAssembly as a compilation target to achieve this goal.

## 0.4.0 - [ ] - Future Functionalities

Other sections will likely be expanded with more goals/functionality as I research more on the topics. This and other placeholders will be used for functionality that does not fit in other sections.

## 1.0 - [ ] Full Functionality

As per my discretion; the project may never reach here, but would be expected to be fully useable as a client at some time before this point.
