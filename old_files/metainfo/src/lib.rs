use bencoder as b;
use bytes::{Bytes, BytesMut};
use std::collections::BTreeMap;
use std::fmt;
use std::str::Utf8Error;
use crypto::{digest::Digest, sha1::Sha1};
use std::path::PathBuf;

// Main error enum for the MetaInfo struct
// contains specific relevant errors, in addition to nested MultiErrors
pub enum MetaError {
    AnnounceErr,            // announce data is missing from the struct (required)
    EncodingErr,            // the string of piece encodings isn't valid, or is missing
    FileLengthErr,          // no file length data was given
    InfoErr,                // no info for the file
    MultiErr(MultiError),   // container error
    NoDict,
    PieceLengthErr,         // no piece length data was given
    UTF8Err(Utf8Error),     // couldn't convert a bytestring from utf8
}

// sub error enum
// contains errors that only occur in multi-file torrents
pub enum MultiError {
    FileDictErr, // no dictionary given for a specific file
    FilesErr,    // no files listed
    PathErr,     // invalid or missing path
}

// formatting for MetaErrors
impl fmt::Display for MetaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MetaError::AnnounceErr      => write!(f, "No Announce URL Found."),
            MetaError::EncodingErr      => write!(f, "Encoded Pieces String Invalid or Missing."),
            MetaError::FileLengthErr    => write!(f, "No File Length Found."),
            MetaError::InfoErr          => write!(f, "No Info Data Found."),
            MetaError::NoDict           => write!(f, "No Dictionary Found."),
            MetaError::MultiErr(e)      => write!(f, "{} in Multiple File Torrent.", e),
            MetaError::PieceLengthErr   => write!(f, "No Piece Length Found."),
            MetaError::UTF8Err(e)       => write!(f, "Couldn't convert to UTF8 with error: {}", e),
        }
    }
}

// formatting for MultiErrors
impl fmt::Display for MultiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MultiError::FileDictErr => write!(f, "No File Dictionary Found"),
            MultiError::FilesErr    => write!(f, "No Files Found"),
            MultiError::PathErr     => write!(f, "No Path Found"),
        }
    }
}

// conversion from built in Utf8Error to custom error type
impl From<Utf8Error> for MetaError {
    fn from(error: Utf8Error) -> Self {
        MetaError::UTF8Err(error)
    }
}

// conversion from specific multi error to MultiErr
impl From<MultiError> for MetaError {
    fn from(error: MultiError) -> Self {
        MetaError::MultiErr(error)
    }
}

// main struct for the torrent metainfo
// all of these fields are keys in a dictionary
// any Option<> type is an optional field
// that can be listed in a .torrent file
#[derive(PartialEq, Eq, Debug, Clone)]
pub struct MetaInfo {
    announce: Bytes,                        // the announced url of the tracker
    announce_list: Option<Vec<Vec<Bytes>>>, // list of list of other announced tracker urls
                                            // also offers backwards compatability
    comment: Option<Bytes>,                 // author's comments
    created_by: Option<Bytes>,              // name and version of the program used to create the torrent
    creation_date: Option<i64>,             // integer, time since unix epoch (seconds)
    encoding: Option<Bytes>,                // encoding format used to generate the 'pieces' section of info
    info: Info,                             // sub dictionary
    info_hash: Bytes,                       // SHA1 hash of the info data
}

impl MetaInfo {
    pub fn get_hash(&self) -> &Bytes {
        &self.info_hash
    }
    
    pub fn get_len(&self) -> Bytes {
        Bytes::from(self.info.length().to_string())
    }

    pub fn get_announce(&self) -> &Bytes {
        &self.announce
    }
}

// info enum (whether the file is a single- or multi-file torrent)
#[derive(PartialEq, Eq, Debug, Clone)]
pub enum Info {
    Single(SingleFile),
    Multiple(MultiFile),
}

impl Info {
    pub fn length(&self) -> i64 {
        match self {
            Info::Single(s)  => s.length(),
            Info::Multiple(m)=> m.length(),
        }
    }
}

// data for single-file torrent
#[derive(PartialEq, Eq, Debug, Clone)]
pub struct SingleFile {
    length: i64,            // length of the full file (bytes)
    md5sum: Option<Bytes>,  // 32 char hex string for the files md5 sum
    shared: SharedData,     // shared data for single and mult files
}

impl SingleFile {
    fn length(&self) -> i64 {
        self.length
    }
}

// data for multi-file torrent
#[derive(PartialEq, Eq, Debug, Clone)]
pub struct MultiFile {
    files: Vec<File>,   // list of dictionaries, one for each file
    shared: SharedData, // shared data for single and mult files
    length: i64,
}

impl MultiFile {
    fn length(&self) -> i64 {
        self.length
    }
}

// shared struct data between single- and multi- files
#[derive(PartialEq, Eq, Debug, Clone)]
pub struct SharedData {
    name: Option<Bytes>,   // filename (single file torrent)
                            // directory name to store all the files (multi file torrent)
    piece_length: i64,      // number of bytes in each piece
    pieces: Bytes,          // byte string of the SHA1 (or other encoding method) hash values of each piece
                            // must be a multiple of 20
    private: bool,          // either 1 (true), or 0/missing in file (false)
                            // dictates the scope of finding peers on a file
}

// list of dictionaries for multi-file torrent
#[derive(PartialEq, Eq, Debug, Clone)]
pub struct File {   // dictionary for a file (multi file torrent)
    length: i64,            // length of the full file (bytes)
    md5sum: Option<Bytes>,  // 32 char hex string for the files md5 sum
    path: PathBuf,      // list of 1+ strings that represent the path
}

impl File {
    fn length(&self) -> i64 {
        self.length
    }
}

// methods for MetaInfo struct
impl MetaInfo {
    // constructor
    pub fn new(dat: &b::Parsed) -> Result<MetaInfo, MetaError> {
        let data = &dat.structure;
        // destructure parsed data from the struct
        if let Some(metadict) = data.get(0) {
            // check to ensure that the data is a dictionary
            match metadict {
                // destructure to get the dictionary
                b::ParseOptions::Dictionary(d) => Ok( // wrapper for our MetaInfo struct
                    // we set fields according to their corresponding values from the dictionary
                    MetaInfo {
                        announce: match MetaInfo::match_string(d, Bytes::from(&b"announce"[..])) {
                            Some(s) => s.to_owned(),                    // if we get a string returned from the query, assign it to announce
                            None => return Err(MetaError::AnnounceErr), // otherwise, no announce was found
                        },
                        announce_list: match MetaInfo::match_list(d, Bytes::from(&b"announce-list"[..])) {
                            Some(l) => Some(l.to_owned()),  // if we get a list from our query, it's the announce list
                            None => None,                   // otherwise no announce list, which is okay
                        },
                        comment: MetaInfo::match_string(d, Bytes::from(&b"comment"[..])),      // assign a comment, if there is one
                        created_by: MetaInfo::match_string(d, Bytes::from(&b"created by"[..])),// assign creator data, if there is any
                        creation_date: match MetaInfo::match_int(d, Bytes::from(&b"creation date"[..])) {
                            Some(i) => Some(i), // if there is a creation date, assign it
                            _ => None,          // okay if there is none
                        },
                        encoding: MetaInfo::match_string(d, Bytes::from(&b"encoding"[..])),// the encoding format (needs more work) 
                        info: match d.get(&Bytes::from(&b"info"[..])) {
                            // matches our query for the info subdictionary
                            Some(b::ParseOptions::Dictionary(e)) => match e.get(&Bytes::from(&b"files"[..])) {
                                None => Info::Single(MetaInfo::set_single(e)?),     // single file mode, helper method is called
                                Some(_) => Info::Multiple(MetaInfo::set_multi(e)?), // multi file mode, helper method is called
                            },
                            // if no dictionary, invalid torrent
                            _ => return Err(MetaError::InfoErr),
                        },
                        info_hash: match d.get(&Bytes::from(&b"info"[..])) {
                            // matches our query for the info subdictionary
                            Some(b::ParseOptions::Dictionary(e)) => MetaInfo::make_hash(e.clone()),
                            // if no dictionary, invalid torrent
                            _ => return Err(MetaError::InfoErr),
                        },
                    }
                ),
                _ => Err(MetaError::NoDict), // if there wasn't a dictionary, set the metainfo as empty
            }
        } else {
            Err(MetaError::NoDict)   // if the parsed data didn't work, set the metainfo as empty
        }
    }

    fn make_hash(info: BTreeMap<Bytes, b::ParseOptions>) -> Bytes {
        // make new hasher
        let mut hasher = Sha1::new();
        // bencode the info dictionary
        let bencoded = b::Parsed::encode_dict(info);
        // new mutable for holding the hashing result
        let mut temp: Vec<u8> = vec![0; 20];
        // input the dictionary
        hasher.input(&bencoded);
        // read hash digest into the temp vector
        hasher.result(&mut temp);
        // feed temp to BytesMut, freeze into Bytes
        BytesMut::from(&temp[..]).freeze()
    }

    // helper method for constructing a single file torrent
    fn set_single(data: &BTreeMap<Bytes, b::ParseOptions>) -> Result<SingleFile, MetaError> {
        Ok( // wrapper for the struct
            SingleFile {// a single file torrent
                length: match MetaInfo::match_int(data, Bytes::from(&b"length"[..])) {
                    Some(i) => i,                                   // destructures the length and assigns it
                    None => return Err(MetaError::FileLengthErr),   // propogates file length error
                },
                md5sum: match data.get(&Bytes::from(&b"md5sum"[..])) {
                    Some(b::ParseOptions::ByteString(bstr)) => Some(bstr.clone()),
                    _ => None,
                },
                shared: MetaInfo::set_shared(data)?,// helper to set the shared metainfo data
            }
        )
    }

    // helper method for constructing a multiple file torrent
    fn set_multi(data: &BTreeMap<Bytes, b::ParseOptions>) -> Result<MultiFile, MetaError> {
        Ok( // wrapper for the struct
            MultiFile { // a multi file torrent
                files: match data.get(&Bytes::from(&b"files"[..])) {
                    Some(b::ParseOptions::List(l)) => MetaInfo::set_files(l)?,  // if there is a list of files
                                                                                // call the helper to assign them
                    _ => return Err(MetaError::MultiErr(MultiError::FilesErr)), // otherwise, propogate the files error
                },
                shared: MetaInfo::set_shared(data)?,// helper to set the shared metainfo data
                length: match data.get(&Bytes::from(&b"files"[..])) {
                    Some(b::ParseOptions::List(l)) => MetaInfo::get_multi_length(l)?,
                    _ => return Err(MetaError::MultiErr(MultiError::FilesErr)), // otherwise, propogate the files error
                },
            }
        )
    }

    fn get_multi_length(data: &Vec<b::ParseOptions>) -> Result<i64, MetaError> {
        let mut temp = 0;
        for file in data {
            if let b::ParseOptions::Dictionary(d) = file {
                match MetaInfo::match_int(d, Bytes::from(&b"length"[..])) {
                    Some(i) => temp += i,   // confirm that file length is provided
                    None => return Err(MetaError::FileLengthErr),
                }
            }
        }
        Ok(temp)
    }

    // helper method for setting up the files in a multiple file torrent
    fn set_files(data: &Vec<b::ParseOptions>) -> Result<Vec<File>, MetaError> {
        // vector for all the files
        let mut files = Vec::new();
        for file in data {  // for each file
            if let b::ParseOptions::Dictionary(d) = file { // confirm that it's a dictionary
                files.push( // pushes a file struct onto the vector
                    File {
                        length: match MetaInfo::match_int(d, Bytes::from(&b"length"[..])) {
                            Some(i) => i,   // confirm that file length is provided
                            None => return Err(MetaError::FileLengthErr),
                        },
                        md5sum: MetaInfo::match_string(d, Bytes::from(&b"md5sum"[..])),// assign if there
                        path: match d.get(&Bytes::from(&b"path"[..])) {
                            Some(b::ParseOptions::List(l)) => MetaInfo::set_path(l)?,   // call helper for the path
                            _ => return Err(MetaError::MultiErr(MultiError::PathErr)),  // otherise return the appropriate error
                        },
                    }
                )
            } else {// if not a dictionary, return the appropriate error
                return Err(MetaError::MultiErr(MultiError::FileDictErr));
            }
        }
        Ok(files)   // wrapper for the files
    }

    // helper method for setting path data in a multiple file torrent
    fn set_path(data: &Vec<b::ParseOptions>) -> Result<PathBuf, MetaError> {
        // create a mutable path buffer
        let mut temp_path = PathBuf::new();
        for section in data {   // for each piece of the path
            if let b::ParseOptions::ByteString(b) = section {
                if let Ok(s) = String::from_utf8(b.to_vec()) {
                    temp_path.push(s)
                } else {
                    return Err(MetaError::MultiErr(MultiError::PathErr));
                }
            } else {    // if invalid piece
                return Err(MetaError::MultiErr(MultiError::PathErr));
            }
        }
        Ok(temp_path)
    }

    // helper method for setting the shared metadata
    fn set_shared(data: &BTreeMap<Bytes, b::ParseOptions>) -> Result<SharedData, MetaError> {
        Ok(
            SharedData {
                name: MetaInfo::match_string(data, Bytes::from(&b"name"[..])), // sets name for either torrent file or directory name
                piece_length: match MetaInfo::match_int(data, Bytes::from(&b"piece length"[..])) { // gets the piece length
                    Some(i) => i.clone(),
                    None => return Err(MetaError::PieceLengthErr),
                },
                // gets the encoded bytestring of pieces
                // typically a string of SHA1 hashes
                // need to restructure for other encoding options (future)
                pieces: match data.get(&Bytes::from(&b"pieces"[..])) {
                    Some(b::ParseOptions::ByteString(s)) => match s.len() % 20 { // used for SHA hash, but could be a different hash
                        0 => s.clone(),
                        _ => return Err(MetaError::EncodingErr),
                    },
                    _ => return Err(MetaError::EncodingErr),// if not a byte string, then 'pieces' is invalid 
                },
                private: match data.get(&Bytes::from(&b"private"[..])) {
                    Some(b::ParseOptions::Integer(1)) => true,  // true for being a private torrent
                    _ => false,                                 // false by default
                },
            }
        )
    }

    // helper method for destructuring and then converting a ByteString to a String
    fn match_string(data: &BTreeMap<Bytes, b::ParseOptions>, input: Bytes) -> Option<Bytes> {
        match data.get(&input) {
            Some(b::ParseOptions::ByteString(bstr)) => Some(bstr.clone()),
            _ => None,
        }
    }

    // helper method for destructuring an Integer
    fn match_int(data: &BTreeMap<Bytes, b::ParseOptions>, input: Bytes) -> Option<i64> {
        match data.get(&input) {
            Some(b::ParseOptions::Integer(i)) => Some(i.clone()),
            _ => None,
        }
    }

    // helper method for destructuring a List of List of Strings
    // used in announce-list
    fn match_list(data: &BTreeMap<Bytes, b::ParseOptions>, input: Bytes) -> Option<Vec<Vec<Bytes>>> {
        let mut temp = Vec::new();
        match data.get(&input) {
            Some(b::ParseOptions::List(l)) => for ele in l {
                match ele {
                    b::ParseOptions::List(li) => for el in li {
                        if let b::ParseOptions::ByteString(bstr) = el {
                            let mut temp1 = Vec::new();
                            temp1.push(bstr.clone());
                            temp.push(temp1);
                        } else {
                            return None;
                        }
                    },
                    _ => return None,
                }
            },
            _ => return None,
        }
        Some(temp)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_single() {
        println!("beginning");
        let input = Bytes::from(&b"d8:announce33:udp://tracker.opentrackr.org:13377:comment12:ManjaroLinux10:created by13:mktorrent 1.113:creation datei1583936924e4:infod6:lengthi0e4:name37:manjaro-kde-19.0.2-200311-linux54.iso12:piece lengthi0e6:pieces0:ee"[..]);
        let parse_input = b::Parsed::new(input);
        println!("after parse bencode");
        if let Ok(parse) = parse_input {
            println!("parsed okay");
            // get structure from parse_input, pass to MetaInfo
            let a = MetaInfo::new(&parse);
            println!("after parse metainfo");
            let b = MetaInfo {
                announce: Bytes::from("udp://tracker.opentrackr.org:1337"),
                announce_list: None,
                comment: Some(Bytes::from("ManjaroLinux")),
                created_by: Some(Bytes::from("mktorrent 1.1")),
                creation_date: Some(1583936924),
                encoding: None,
                info: Info::Single(
                    SingleFile {
                        length: 0,
                        md5sum: None,
                        shared: SharedData {
                            name: Some(Bytes::from("manjaro-kde-19.0.2-200311-linux54.iso")),
                            piece_length: 0,
                            pieces: Bytes::from(&b""[..]),
                            private: false,
                        },
                    }
                ),
                info_hash: Bytes::from(&b"\xa0\x0e`\xf7\xc5v\x18\x12\x90\x99^\x1f\x11BOVg\x99\xc38"[..]),
            };
            match a {
                Ok(c) => assert_eq!(c, b),
                Err(e) => panic!("Test failed with error: {}", e.to_string()),
            }
        } else {
            panic!("Could not parse the input");
        }
    }

    #[test]
    fn test_multi() {
        let input = Bytes::from(&b"d8:announce35:https://torrent.ubuntu.com/announce13:announce-listll35:https://torrent.ubuntu.com/announceel40:https://ipv6.torrent.ubuntu.com/announceee7:comment29:Ubuntu CD releases.ubuntu.com13:creation datei1581514916e4:infod5:filesld6:lengthi0e4:pathl4:dir1eee4:name32:ubuntu-18.04.4-desktop-amd64.iso12:piece lengthi0e6:pieces0:ee"[..]);
        let parse_input = b::Parsed::new(input);
        if let Ok(parse) = parse_input {
            // get structure from parse_input, pass to MetaInfo
            let a = MetaInfo::new(&parse);
            let b = MetaInfo {
                announce: Bytes::from("https://torrent.ubuntu.com/announce"),
                announce_list: Some(
                    vec![
                        vec![Bytes::from("https://torrent.ubuntu.com/announce")],
                        vec![Bytes::from("https://ipv6.torrent.ubuntu.com/announce")],
                    ]
                ),
                comment: Some(Bytes::from("Ubuntu CD releases.ubuntu.com")),
                created_by: None,
                creation_date: Some(1581514916),
                encoding: None,
                info: Info::Multiple(
                    MultiFile {
                        files: vec![
                            File {
                                length: 0,
                                md5sum: None,
                                path: PathBuf::from("dir1"),
                            }
                        ],
                        shared: SharedData {
                            name: Some(Bytes::from("ubuntu-18.04.4-desktop-amd64.iso")),
                            piece_length: 0,
                            pieces: Bytes::from(&b""[..]),
                            private: false,
                        },
                        length: 0,
                    }
                ),
                info_hash: Bytes::from(&b"\xc7\xd8\xb7[RI\x83V*\t\xb0f\xe2\xfdh<\xcf\xd4\xa8r"[..]),
            };
            match a {
                Ok(c) => assert_eq!(c, b),
                Err(e) => panic!("Test failed with error: {}", e.to_string()),
            }
        } else {
            panic!("Couldn't parse the input");
        }
    }
}