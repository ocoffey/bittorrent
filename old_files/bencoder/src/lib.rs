use bytes::{BytesMut, Bytes};
use std::vec::Vec;
use std::collections::BTreeMap;
use std::fmt;
use std::str::Utf8Error;
use std::num::ParseIntError;
use std::convert::From;

// different parsed options for data
// dictionaries and lists can contain each other
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum ParseOptions {
    Integer(i64),
    ByteString(Bytes),
    Dictionary(BTreeMap<Bytes, ParseOptions>),
    List(Vec<ParseOptions>),
}

// formatting for the parsed options
impl fmt::Display for ParseOptions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            ParseOptions::Integer(int)              => write!(f, "{}", int),
            ParseOptions::ByteString(b_string)      => write!(f, "{}", String::from_utf8_lossy(b_string).to_string()),
            ParseOptions::List(list)                => self.fmt_list(f, list, 0),
            ParseOptions::Dictionary(dict)          => self.fmt_dict(f, dict, 0),
        }
    }
}

// formatting help for lists and dictionaries,
// since each have recursive capability
impl ParseOptions {
    // recursive list formatting
    fn fmt_list(&self, f: &mut fmt::Formatter<'_>, l: &[ParseOptions], space: usize) -> fmt::Result {
        for ele in l {
            for _ in 0..space {write!(f, " ")?;}
            match ele {
                ParseOptions::List(list) =>
                    if list.is_empty() {
                        writeln!(f, "<Empty List>")?;
                    } else if list.len() == 1 {
                        if let Some(e) = list.get(0) {
                            writeln!(f, " * {}", e)?;
                        }
                    } else {
                        self.fmt_list(f, list, space + 1)?;
                    },
                ParseOptions::Dictionary(dict) =>
                    if dict.is_empty() {
                        writeln!(f, "* <Empty Dictionary>")?;
                    } else if dict.len() == 1 {
                        for (key, val) in dict {writeln!(f, "* {}: {}", String::from_utf8_lossy(key), val)?;}
                    } else {
                        write!(f, "*")?;
                        self.fmt_dict(f, dict, space + 2)?;
                    },
                _ => {writeln!(f, "* {}", ele)?; ()},
            }
        }
        write!(f, "")
    }
    // recursive dictionary formatting
    fn fmt_dict(&self, f: &mut fmt::Formatter<'_>, d: &BTreeMap<Bytes, ParseOptions>, space: usize) -> fmt::Result{
        for _ in 0..space {write!(f, " ")?;}
        writeln!(f, "{{")?;
        let space = space + 2;
        if d.len() == 1 {
            for _ in 0..space {write!(f, " ")?;}
            for (key, val) in d {write!(f, "{}: {}", String::from_utf8_lossy(key), val)?;}
            for _ in 0..space - 2 {write!(f, " ")?;}
            writeln!(f, "}}")?;
        }
        else {
            for (key, val) in d {
                for _ in 0..space {write!(f, " ")?;}
                write!(f, "{}:", String::from_utf8_lossy(key))?;
                match val {
                    ParseOptions::List(list) => {
                        if list.is_empty() {
                            writeln!(f, " <Empty List>")?;
                        } else if list.len() == 1 {
                            if let Some(e) = list.get(0) {
                                writeln!(f, " * {}", e)?;
                            }
                        } else {
                            if let Some(e) = list.get(0) {
                                writeln!(f, " * {}", e)?;
                            }
                            self.fmt_list(f, &list[1..], space + key.len() + 2)?;
                        }
                    },
                    ParseOptions::Dictionary(dict) => {
                        if dict.is_empty() {
                            writeln!(f, " <Empty Dictionary>")?;
                        } else {
                            self.fmt_dict(f, dict, space + key.len() + 6)?;
                        }
                    },
                    _ => {writeln!(f, " {}", val)?;},
                }
            }
            for _ in 0..space - 2 {write!(f, " ")?;} 
            writeln!(f, "}}")?;
        }
        write!(f, "")
    }   
}

// Error options
// SliceErr comes from .get() use, and contains the index that the error occurred in
// InvalidData comes from a couple of areas, namely elements appearing where they shouldn't
    // (a non-number inbetween an 'i' and an 'e', for example)
    // contains the index of the error
// UTF8Err comes from parsing Bytes to strings
    // mostly used in part when converting Bytes to strings to ints
    // contains the error index
// ParseErr comes from parsing a string to an int
    // occurs in the decode_integer method
    // contains the error index
pub enum ParseError {
    SliceErr(usize),
    InvalidData(usize),
    UTF8Err(usize, Utf8Error),
    ParseErr(usize, ParseIntError),
}

// fancy error printing
impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            ParseError::SliceErr(index)         => write!(f, "Couldn't Get Slice at Index: {}", index),
            ParseError::InvalidData(index)      => write!(f, "Invalid Input Data at Index: {}", index),
            ParseError::UTF8Err(index, error)   => write!(f, "Failure to Convert from UTF8 at index {} with error: {}", index, error),
            ParseError::ParseErr(index, error)  => write!(f, "Failure to Parse String at index {} with error: {}", index, error),
        }
    }
}

// derivations from existing error types
impl From<Utf8Error> for ParseError {
    fn from(error: Utf8Error) -> Self {
        ParseError::UTF8Err(0, error)
    }
}

impl From<ParseIntError> for ParseError {
    fn from(error: ParseIntError) -> Self {
        ParseError::ParseErr(0, error)
    }
}

// main struct for holding the data
// structure contains the parsed data as a recursive struct
pub struct Parsed {
    pub structure: Vec<ParseOptions>,
}

// for printing the structure
impl fmt::Display for Parsed {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // for each element in the structure, in order
        for element in &self.structure {
            writeln!(f, "{}", element)?;
        }
        Ok(())
    }
}

// methods for Parsed struct
impl Parsed{
    // constructor, takes the original Bytes and throws it at instructions
    pub fn new(input: Bytes) -> Result<Parsed, ParseError> {
        Ok(Parsed{
            structure: Parsed::decode(input)?,
        })
    }

    // public method for decoding the data
    fn decode(instructions: Bytes) -> Result<Vec<ParseOptions>, ParseError>{
        // total input length
        let length: usize = instructions.len();
        // counter for walking through torrent bytes
        let mut walk: usize = 0;
        let mut temp = Vec::new();
        while walk < length {
            match instructions.get(walk) {
                Some(b'i') => match Parsed::decode_integer(&instructions.slice(walk..)) {
                    Ok((length, ret_int)) => {walk += length + 2;
                        temp.push(ParseOptions::Integer(ret_int));},
                    Err(e) => return Err(Parsed::match_errors(e, walk)),
                },
                Some(b'0'..=b'9') => match Parsed::decode_string(&instructions.slice(walk..)) {
                    Ok((length, ret_str)) => {walk += length;
                        temp.push(ParseOptions::ByteString(ret_str));},
                    Err(e) => return Err(Parsed::match_errors(e, walk)),
                },
                Some(b'l') => match Parsed::decode_list(&instructions.slice(walk..)) {
                    Ok((length, ret_list)) => {walk += length;
                        temp.push(ParseOptions::List(ret_list));},
                    Err(e) => return Err(Parsed::match_errors(e, walk)),
                },
                Some(b'd') => match Parsed::decode_dict(&instructions.slice(walk..)) {
                    Ok((length, ret_dict)) => {walk += length;
                        temp.push(ParseOptions::Dictionary(ret_dict));},
                    Err(e) => return Err(Parsed::match_errors(e, walk)),
                },
                Some(_) => return Err(ParseError::InvalidData(walk)),
                None => return Err(ParseError::SliceErr(walk)),
            }
        }
        Ok(temp)
    }

    // helper method for matching errors, and returning appropriate error index
    fn match_errors(error: ParseError, index: usize) -> ParseError {
        match error {
            ParseError::InvalidData(pwalk)  => ParseError::InvalidData(index + pwalk),
            ParseError::SliceErr(pwalk)     => ParseError::SliceErr(index + pwalk),
            ParseError::UTF8Err(pwalk, e)   => ParseError::UTF8Err(index + pwalk, e),
            ParseError::ParseErr(pwalk, e)  => ParseError::ParseErr(index + pwalk, e),
        }
    }

    fn decode_integer(integer: &Bytes) ->  Result<(usize, i64), ParseError> {
        match integer.get(0) {
            // if our first character is 'i'
            Some(b'i') => {match integer.get(1) {
                    // if the second char is "-"
                    // check for it being postceeded by 0 or e
                    // if it is, return an error
                    Some(b'-') => match integer.get(2) {
                        Some(b'0') | Some(b'e') => return Err(ParseError::InvalidData(2)),
                        Some(_) => (),
                        None => return Err(ParseError::SliceErr(2)),
                    },
                    // if the second char is "0"
                    Some(b'0') => match integer.get(2) {
                        // if it's 'i0e', return length 3, and number 0
                        Some(b'e') => return Ok((3, 0)),
                        // throw an error for anything that isn't 'e'
                        Some(_) => return Err(ParseError::InvalidData(2)),
                        None => return Err(ParseError::SliceErr(2)),
                    },
                    // if our string has 'i' followed by 'e', throw error
                    Some(b'e') => return Err(ParseError::InvalidData(1)),
                    // if it's a number in range, it's okay
                    Some(b'1'..=b'9') => (),
                    // Anything else is an error
                    Some(_) => return Err(ParseError::InvalidData(1)),
                    None => return Err(ParseError::SliceErr(1)),
                }
                // walk until an error, or until we hit 'e'
                // not done walking
                let mut done = false;
                // counter for digits
                // should only be 63 binary digits long, maximum
                let mut digits: usize = 0;
                // walk
                while !done && digits < 20 {
                    match integer.get(digits + 2) {
                        // increment number of digits, if valid digit
                        Some(b'0'..=b'9') => digits += 1,
                        Some(b'e')        => done = true,
                        Some(_)           => return Err(ParseError::InvalidData(digits + 2)),
                        None              => return Err(ParseError::SliceErr(digits + 2)),
                    }
                }
                // when done, convert it into a number and return it
                if let Some(number) = integer.get(1..digits+2) {
                    Ok((digits+3, std::str::from_utf8(number)?.parse::<i64>()?))
                } else {
                    Err(ParseError::SliceErr(digits + 2))
                }},
            Some(_) => Err(ParseError::InvalidData(0)),
            // invalid index
            None => Err(ParseError::SliceErr(0)),
        }
    }

    fn decode_string(bytstr: &Bytes) ->  Result<(usize, Bytes), ParseError> {
        // ensure that this is a byte string
        match bytstr.get(0) {
            // if first char is 0, then next has to be ':', else invalid
            Some(b'0') => match bytstr.get(1) {
                Some(b':') => return Ok((2, Bytes::from(""))),
                Some(_) => return Err(ParseError::InvalidData(1)),
                None => return Err(ParseError::SliceErr(1)),
            },
            // if non-zero, keep going to get the length
            Some(b'1'..=b'9') => (),
            // any invalid char or 'None' is invalid
            Some(_) => return Err(ParseError::InvalidData(0)),
            None => return Err(ParseError::SliceErr(0))
        }
        // keep going until ':'
        let mut digits = 0;
        loop {
            match bytstr.get(digits) {
                Some(b':') => break,
                Some(b'0'..=b'9') => digits += 1,
                Some(_) => return Err(ParseError::InvalidData(digits)),
                None => return Err(ParseError::SliceErr(digits)),
            }
        }
        // get all Bytes until the ':' as a [u8],
        if let Some(s) = bytstr.get(0..digits) {
            // convert to a string, then parse to a number
            let string_dig = std::str::from_utf8(s)?.parse::<usize>()?;
            // if we have a valid slice
            if let Some(_) = bytstr.get(digits + 1..digits + 1 + string_dig) {
                // return it
                Ok((digits + string_dig + 1, bytstr.slice(digits + 1..digits + 1 + string_dig)))
            } else {
                // invalid slice (probably out of bounds)
                Err(ParseError::SliceErr(digits + 1 + string_dig))
            }
        } else {
            // invalid slice (probably out of bounds)
            Err(ParseError::SliceErr(digits))
        }
    }

    fn decode_list(list: &Bytes) ->  Result<(usize, Vec<ParseOptions>), ParseError> {
        let mut temp_list = Vec::new();
        // match characters
        // call other methods if you encounter the specific delimiters
        match list.get(0) {
            Some(b'l') => {
                let mut walk = 1;
                loop {
                    match list.get(walk) {
                        Some(b'e') => return Ok((walk + 1, temp_list)),
                        Some(b'0'..=b'9') => match Parsed::decode_string(&list.slice(walk..)) {
                            Ok((length, byt_str)) => {walk += length;
                                temp_list.push(ParseOptions::ByteString(byt_str));
                            },
                            Err(e) => return Err(Parsed::match_errors(e, walk)),
                        },
                        Some(b'i') => match Parsed::decode_integer(&list.slice(walk..)) {
                            Ok((length, number)) => {walk += length;
                                temp_list.push(ParseOptions::Integer(number));},
                            Err(e) => return Err(Parsed::match_errors(e, walk)),
                        },
                        Some(b'd') => match Parsed::decode_dict(&list.slice(walk..)) {
                            Ok((length, ret_dict)) => {walk += length;
                                temp_list.push(ParseOptions::Dictionary(ret_dict));},
                            Err(e) => return Err(Parsed::match_errors(e, walk)),
                        },
                        Some(b'l') => match Parsed::decode_list(&list.slice(walk..)) {
                            Ok((length, ret_list)) => {walk += length;
                                temp_list.push(ParseOptions::List(ret_list));},
                            Err(e) => return Err(Parsed::match_errors(e, walk)),
                        },
                        Some(_) => return Err(ParseError::InvalidData(walk)),
                        None => return Err(ParseError::SliceErr(walk)),
                    }
                }
            },
            Some(_) => Err(ParseError::InvalidData(0)),
            None => Err(ParseError::SliceErr(0)),
        }
    }

    fn decode_dict(dict: &Bytes) ->  Result<(usize, BTreeMap<Bytes, ParseOptions>), ParseError> {
        // can hold everything
        // keys must be strings
        match dict.get(0) {
            Some(b'd') => {
                // make new BTreeMap
                let mut temp_dict = BTreeMap::new();
                // alternating boolean that tells us when to insert a key-value pair
                let mut value = false;
                // counter var
                let mut walk = 1;
                // blank key
                let mut key = Bytes::from("");
                // loop getting key value pairs
                loop {
                    match dict.get(walk) {
                        // when you hit 'e', return the dictionary
                        Some(b'e') => return Ok((walk + 1, temp_dict)),
                        Some(_) => {
                            // if looking for the key, get a string
                            if !value {
                                match Parsed::decode_string(&dict.slice(walk..)) {
                                    Ok((length, byte_string)) => {
                                        walk += length;
                                        key = byte_string;
                                    },
                                    Err(e) => return Err(Parsed::match_errors(e, walk)),
                                }
                                value = true;
                            } else {
                                match dict.get(walk) {
                                    Some(b'0'..=b'9') => match Parsed::decode_string(&dict.slice(walk..)) {
                                        Ok((length, byt_str)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::ByteString(byt_str));},
                                        Err(e) => return Err(Parsed::match_errors(e, walk)),
                                    },
                                    Some(b'i') => match Parsed::decode_integer(&dict.slice(walk..)) {
                                        Ok((length, number)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::Integer(number));},
                                        Err(e) => return Err(Parsed::match_errors(e, walk)),
                                    },
                                    Some(b'l') => match Parsed::decode_list(&dict.slice(walk..)) {
                                        Ok((length, ret_list)) => {walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::List(ret_list));},
                                        Err(e) => return Err(Parsed::match_errors(e, walk)),
                                    },
                                    Some(b'd') => match Parsed::decode_dict(&dict.slice(walk..)) {
                                        Ok((length, ret_dict)) => {
                                            walk += length;
                                            temp_dict.insert(key.clone(), ParseOptions::Dictionary(ret_dict));},
                                        Err(e) => return Err(Parsed::match_errors(e, walk)),
                                    },
                                    Some(_) => return Err(ParseError::InvalidData(walk)),
                                    None => return Err(ParseError::SliceErr(walk)),
                                }
                                value = false;
                            }
                        },
                        None => return Err(ParseError::SliceErr(walk)),
                    }
                }
            },
            Some(_) => Err(ParseError::InvalidData(0)),
            None => Err(ParseError::SliceErr(0)),
        }
    }


    // encoder for struct
    pub fn encode(self) -> Bytes {
        let mut temp = BytesMut::new();
        for val in self.structure {
            match val {
                ParseOptions::Integer(i) => temp.extend_from_slice(&Parsed::encode_int(i)),
                ParseOptions::ByteString(bstr) => temp.extend_from_slice(&Parsed::encode_str(bstr)),
                ParseOptions::List(l) => temp.extend_from_slice(&Parsed::encode_list(l)),
                ParseOptions::Dictionary(d) => temp.extend_from_slice(&Parsed::encode_dict(d)),
            }
        }
        temp.freeze()
    }

    fn encode_int(int: i64) -> Bytes {
        let mut temp = BytesMut::new();
        temp.extend_from_slice(b"i");
        temp.extend_from_slice(&Bytes::from(int.to_string()));
        temp.extend_from_slice(b"e");
        temp.freeze()
    }

    fn encode_str(bstr: Bytes) -> Bytes {
        let mut temp = BytesMut::new();
        temp.extend_from_slice(&Bytes::from(bstr.len().to_string()));
        temp.extend_from_slice(b":");
        temp.extend_from_slice(&bstr);
        temp.freeze()
    }

    fn encode_list(list: Vec<ParseOptions>) -> Bytes {
        let mut temp = BytesMut::new();
        temp.extend_from_slice(b"l");
        for ele in list {
            match ele {
                ParseOptions::Integer(i) => temp.extend_from_slice(&Parsed::encode_int(i)),
                ParseOptions::ByteString(bstr) => temp.extend_from_slice(&Parsed::encode_str(bstr)),
                ParseOptions::List(l) => temp.extend_from_slice(&Parsed::encode_list(l)),
                ParseOptions::Dictionary(d) => temp.extend_from_slice(&Parsed::encode_dict(d)),
            }
        }
        temp.extend_from_slice(b"e");
        temp.freeze()
    }

    pub fn encode_dict(dict: BTreeMap<Bytes, ParseOptions>) -> Bytes {
        let mut temp = BytesMut::new();
        temp.extend_from_slice(b"d");
        for (key, val) in dict {
            temp.extend_from_slice(&Parsed::encode_str(key));
            match val {
                ParseOptions::Integer(i) => temp.extend_from_slice(&Parsed::encode_int(i)),
                ParseOptions::ByteString(bstr) => temp.extend_from_slice(&Parsed::encode_str(bstr)),
                ParseOptions::List(l) => temp.extend_from_slice(&Parsed::encode_list(l)),
                ParseOptions::Dictionary(d) => temp.extend_from_slice(&Parsed::encode_dict(d)),
            }
        }
        temp.extend_from_slice(b"e");
        temp.freeze()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn int_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // tests
        inputs.push((Bytes::from(&b"i0e"[..]), true, 0));
        inputs.push((Bytes::from(&b"i2022e"[..]), true, 2022));
        inputs.push((Bytes::from(&b"i-14e"[..]), true, -14));
        inputs.push((Bytes::from(&b"i92034923049923e"[..]), true, 92034923049923 as i64));
        inputs.push((Bytes::from(&b"ie"[..]), false, 0));
        inputs.push((Bytes::from(&b"i-0e"[..]), false, 0));
        inputs.push((Bytes::from(&b"i-e"[..]), false, 0));
        inputs.push((Bytes::from(&b"i01e"[..]), false, 0));
        // for each input
        for (byte_input, valid, expect_out) in inputs {
            // make new bencoder object
            let my_file = Parsed::new(byte_input);
            // test validity
            if valid {
                // if the parsing was valid
                match my_file {
                    // if we were returned the appropriate result
                    // check that result for validity
                    Ok(a) => match a.structure.get(0) {
                        Some(ParseOptions::Integer(i)) => assert_eq!(i, &expect_out),
                        _ => panic!("Invalid Type"),
                    },
                    // gave Err when should have given Ok
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match my_file {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
        }
    }

    #[test]
    fn string_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // different tests
        inputs.push((Bytes::from(&b"5:hello"[..]), true, Bytes::from("hello")));
        inputs.push((Bytes::from(&b"3:hey"[..]), true, Bytes::from("hey")));
        inputs.push((Bytes::from(&b"10:this works"[..]), true, Bytes::from("this works")));
        inputs.push((Bytes::from(&b"0:"[..]), true, Bytes::from("")));
        inputs.push((Bytes::from(&b"05:hello"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"20:hi"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"1:"[..]), false, Bytes::from("")));
        inputs.push((Bytes::from(&b"-0:"[..]), false, Bytes::from("")));
        // for each input
        // has the input, whether the test is valid,
        // the expected output, and the expected length of the output
        for (byte_input, valid, expect_out) in inputs {
            // create a new bencoder object
            // parses in the constructor
            let my_file = Parsed::new(byte_input);
            // if the test is valid
            if valid {
                match my_file {
                    // with valid output
                    Ok(a) => match a.structure.get(0) {
                        // we should have a byte string, and can assert it equal to our expected output
                        Some(ParseOptions::ByteString(b)) => assert_eq!(*b, expect_out),
                        // should only be the above case, so panic otherwise
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                match my_file {
                    // with valid output
                    Ok(_) => panic!("Invalid Output"),
                    Err(_) => (),
                }
            }
        }
    }

    #[test]
    fn list_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push((Bytes::from(&b"l12:Grocery List4:Eggs4:Milk5:Breade"[..]), true));
        inputs.push((Bytes::from(&b"l7:Numbers3:Onei2ei3e4:Foure"[..]), true));
        inputs.push((Bytes::from(&b"l6:Silver3:And4:Gold5:Never3:Get3:Olde"[..]), true));
        inputs.push((Bytes::from(&b"le"[..]), true));
        inputs.push((Bytes::from(&b"l1:Electronics6:Laptop5:Phonele"[..]), false));
        inputs.push((Bytes::from(&b"lle"[..]), false));
        inputs.push((Bytes::from(&b"l"[..]), false));
        inputs.push((Bytes::from(&b"l6:Invalide"[..]), false));

        for (byte_input, valid) in inputs {
            // parse into new object
            let my_file = Parsed::new(byte_input);
            if valid {
                match my_file {
                    Ok(a) => match a.structure.get(0) {
                        Some(ParseOptions::List(_)) => (),
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match my_file {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
            
        }
    }

    #[test]
    fn dict_decode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push((Bytes::from(&b"d4:frog9:amphibian3:cat6:felinee"[..]), true));
        inputs.push((Bytes::from(&b"de"[..]), true));
        inputs.push((Bytes::from(&b"d3:onei1e3:twoi2e5:threei3ee"[..]), true));
        inputs.push((Bytes::from(&b"d0:5:empty1:14:fulle"[..]), true));
        inputs.push((Bytes::from(&b"di1e3:onei2e3:two"[..]), false));
        inputs.push((Bytes::from(&b"dde"[..]), false));
        inputs.push((Bytes::from(&b"dde7:invalide"[..]), false));
        inputs.push((Bytes::from(&b"d3:one5:validi2e7:invalide"[..]), false));
        for (byte_input, valid) in inputs {
            // parse into new object
            let my_file = Parsed::new(byte_input);
            if valid {
                match my_file {
                    Ok(a) => match a.structure.get(0) {
                        Some(ParseOptions::Dictionary(_)) => (),
                        _ => panic!("Invalid Type"),
                    },
                    Err(e) => panic!("Invalid Output with error: {}", e),
                }
            } else {
                // if invalid
                match my_file {
                    Err(_) => (),
                    Ok(_) => panic!("Invalid Output"),
                }
            }
            
        }
    }

    #[test]
    fn int_encode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // tests
        inputs.push(Bytes::from(&b"i0e"[..]));
        inputs.push(Bytes::from(&b"i2022e"[..]));
        inputs.push(Bytes::from(&b"i-14e"[..]));
        inputs.push(Bytes::from(&b"i92034923049923e"[..]));
        // for each input
        for input in inputs {
            // make new bencoder object
            let my_file = Parsed::new(input.clone());
            // test validity
            match my_file {
                // if we were returned the appropriate result
                // check that result for validity
                Ok(a) => assert_eq!(a.encode(), input),
                // gave Err when should have given Ok
                Err(e) => panic!("Invalid Output with error: {}", e),
            }
        }
    }

    #[test]
    fn string_encode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // different tests
        inputs.push(Bytes::from(&b"5:hello"[..]));
        inputs.push(Bytes::from(&b"3:hey"[..]));
        inputs.push(Bytes::from(&b"10:this works"[..]));
        inputs.push(Bytes::from(&b"0:"[..]));
        // for each input
        // has the input, whether the test is valid,
        // the expected output, and the expected length of the output
        for input in inputs {
            // create a new bencoder object
            // parses in the constructor
            let my_file = Parsed::new(input.clone());
            match my_file {
                // with valid output
                Ok(a) => assert_eq!(a.encode(), input),
                Err(e) => panic!("Invalid Output with error: {}", e),
            }
        }
    }

    #[test]
    fn list_encode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push(Bytes::from(&b"l12:Grocery List4:Eggs4:Milk5:Breade"[..]));
        inputs.push(Bytes::from(&b"l7:Numbers3:Onei2ei3e4:Foure"[..]));
        inputs.push(Bytes::from(&b"l6:Silver3:And4:Gold5:Never3:Get3:Olde"[..]));
        inputs.push(Bytes::from(&b"le"[..]));

        for input in inputs {
            // parse into new object
            let my_file = Parsed::new(input.clone());
            match my_file {
                Ok(a) => assert_eq!(a.encode(), input),
                Err(e) => panic!("Invalid Output with error: {}", e),
            }
        }
    }

    #[test]
    fn dict_encode_test() {
        // vector to hold sample inputs
        let mut inputs = Vec::new();
        // test inputs
        inputs.push(Bytes::from(&b"d3:cat6:feline4:frog9:amphibiane"[..]));
        inputs.push(Bytes::from(&b"de"[..]));
        inputs.push(Bytes::from(&b"d3:onei1e5:threei3e3:twoi2ee"[..]));
        inputs.push(Bytes::from(&b"d0:5:empty1:14:fulle"[..]));
        for input in inputs {
            // parse into new object
            let my_file = Parsed::new(input.clone());
            match my_file {
                Ok(a) => assert_eq!(a.encode(), input),
                Err(e) => panic!("Invalid Output with error: {}", e),
            }
        }
    }
}
