# Bittorrent

Creating a fully functional bittorrent client in Rust

> It's been some years since I've worked on this project, and I had not finished the initial version as it stands. Many aspects of life have occurred and changed along the way, including my knowledge on how an app like this should work in the first place. This reboot will attempt to complete the initial work, while also using better programming fundamentals such as asynchronous methods for file I/O and networking, among others. The previous project stalled in the tracker request section, I believe due to an improper hash, but I had not debugged it thoroughly to find out if that was truly the case. Regardless of then, this is now. Time to begin :D

## 0.1 - [ ] - Completing the Bittorrent Specification

As described [here](https://wiki.theory.org/BitTorrentSpecification). The specification is for the 1.0 bittorrent protocol.

### 0.1.1 - [ ] - Bencoding Parser

Functionality for parsing bencoded input to a struct. 

### 0.1.2 - [ ] - MetaInfo Struct

A struct that can take bencoding parsed input and returns a relevant struct for a bittorrent client.

### 0.1.3 - [ ] - HTTP/HTTPS Protocol Support

Functionality for sending appropriately formatted GET requests, and making sense of the server's responses. Used when starting a bittorrent download, and throughout the downloading and uploading processes.

#### 0.1.3.6 - [ ] - Tracker Scrape Convention

Support for scrape requests to trackers. Used for updating peer data in the client (ie. amount of seeders and leechers per torrent).
